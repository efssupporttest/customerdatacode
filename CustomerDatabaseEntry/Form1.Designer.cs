﻿namespace CustomerDatabaseEntry
{
    partial class Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form));
            this.customerList = new System.Windows.Forms.ComboBox();
            this.pushChanges = new System.Windows.Forms.Button();
            this.DMVerText = new System.Windows.Forms.TextBox();
            this.ClientVerText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.GSAGen = new System.Windows.Forms.CheckBox();
            this.GSABes = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.BSMBes = new System.Windows.Forms.CheckBox();
            this.BSMGen = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TreeBes = new System.Windows.Forms.CheckBox();
            this.TreeGen = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.DocCorBes = new System.Windows.Forms.CheckBox();
            this.DocCorGen = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.BatchBes = new System.Windows.Forms.CheckBox();
            this.BatchGen = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.PhotoBes = new System.Windows.Forms.CheckBox();
            this.PhotoGen = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.eSigBes = new System.Windows.Forms.CheckBox();
            this.eSigGen = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.AdHocBes = new System.Windows.Forms.CheckBox();
            this.AdHocGen = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.OtherApps = new System.Windows.Forms.TextBox();
            this.statusBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.LNVerText = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.ProdSerText = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.TestSerText = new System.Windows.Forms.TextBox();
            this.passwordText = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.LogInButton = new System.Windows.Forms.Button();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.tableName = new System.Windows.Forms.ComboBox();
            this.TabsWindow = new System.Windows.Forms.TabControl();
            this.swDetails = new System.Windows.Forms.TabPage();
            this.importFromFile = new System.Windows.Forms.Button();
            this.saveToFile = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.notesText = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.BPMVerText = new System.Windows.Forms.TextBox();
            this.customerDetails = new System.Windows.Forms.TabPage();
            this.createCustomer = new System.Windows.Forms.Button();
            this.changeName = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.newCustName = new System.Windows.Forms.TextBox();
            this.lastChangedText = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.outOfSupport = new System.Windows.Forms.Button();
            this.TabsWindow.SuspendLayout();
            this.swDetails.SuspendLayout();
            this.customerDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // customerList
            // 
            this.customerList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.customerList.FormattingEnabled = true;
            this.customerList.Location = new System.Drawing.Point(282, 43);
            this.customerList.Name = "customerList";
            this.customerList.Size = new System.Drawing.Size(167, 21);
            this.customerList.TabIndex = 2;
            this.customerList.SelectedIndexChanged += new System.EventHandler(this.customerList_SelectedIndexChanged);
            // 
            // pushChanges
            // 
            this.pushChanges.Location = new System.Drawing.Point(291, 576);
            this.pushChanges.Name = "pushChanges";
            this.pushChanges.Size = new System.Drawing.Size(108, 25);
            this.pushChanges.TabIndex = 3;
            this.pushChanges.Text = "Push Changes";
            this.pushChanges.UseVisualStyleBackColor = true;
            this.pushChanges.Click += new System.EventHandler(this.pushChanges_Click_1);
            // 
            // DMVerText
            // 
            this.DMVerText.Location = new System.Drawing.Point(6, 133);
            this.DMVerText.MaxLength = 10;
            this.DMVerText.Name = "DMVerText";
            this.DMVerText.Size = new System.Drawing.Size(67, 20);
            this.DMVerText.TabIndex = 9;
            // 
            // ClientVerText
            // 
            this.ClientVerText.Location = new System.Drawing.Point(6, 289);
            this.ClientVerText.MaxLength = 10;
            this.ClientVerText.Name = "ClientVerText";
            this.ClientVerText.Size = new System.Drawing.Size(67, 20);
            this.ClientVerText.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "DM Version";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 273);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Client Version";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(288, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Bespoke";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(215, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Generic";
            // 
            // GSAGen
            // 
            this.GSAGen.AutoSize = true;
            this.GSAGen.Location = new System.Drawing.Point(218, 52);
            this.GSAGen.Name = "GSAGen";
            this.GSAGen.Size = new System.Drawing.Size(15, 14);
            this.GSAGen.TabIndex = 13;
            this.GSAGen.UseVisualStyleBackColor = true;
            // 
            // GSABes
            // 
            this.GSABes.AutoSize = true;
            this.GSABes.Location = new System.Drawing.Point(291, 52);
            this.GSABes.Name = "GSABes";
            this.GSABes.Size = new System.Drawing.Size(15, 14);
            this.GSABes.TabIndex = 14;
            this.GSABes.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(129, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "GSA";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(129, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "BS&M";
            // 
            // BSMBes
            // 
            this.BSMBes.AutoSize = true;
            this.BSMBes.Location = new System.Drawing.Point(291, 80);
            this.BSMBes.Name = "BSMBes";
            this.BSMBes.Size = new System.Drawing.Size(15, 14);
            this.BSMBes.TabIndex = 16;
            this.BSMBes.UseVisualStyleBackColor = true;
            // 
            // BSMGen
            // 
            this.BSMGen.AutoSize = true;
            this.BSMGen.Location = new System.Drawing.Point(218, 80);
            this.BSMGen.Name = "BSMGen";
            this.BSMGen.Size = new System.Drawing.Size(15, 14);
            this.BSMGen.TabIndex = 15;
            this.BSMGen.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(129, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Tree View";
            // 
            // TreeBes
            // 
            this.TreeBes.AutoSize = true;
            this.TreeBes.Location = new System.Drawing.Point(291, 139);
            this.TreeBes.Name = "TreeBes";
            this.TreeBes.Size = new System.Drawing.Size(15, 14);
            this.TreeBes.TabIndex = 20;
            this.TreeBes.UseVisualStyleBackColor = true;
            // 
            // TreeGen
            // 
            this.TreeGen.AutoSize = true;
            this.TreeGen.Location = new System.Drawing.Point(218, 139);
            this.TreeGen.Name = "TreeGen";
            this.TreeGen.Size = new System.Drawing.Size(15, 14);
            this.TreeGen.TabIndex = 19;
            this.TreeGen.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(129, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Doc Correct";
            // 
            // DocCorBes
            // 
            this.DocCorBes.AutoSize = true;
            this.DocCorBes.Location = new System.Drawing.Point(291, 111);
            this.DocCorBes.Name = "DocCorBes";
            this.DocCorBes.Size = new System.Drawing.Size(15, 14);
            this.DocCorBes.TabIndex = 18;
            this.DocCorBes.UseVisualStyleBackColor = true;
            // 
            // DocCorGen
            // 
            this.DocCorGen.AutoSize = true;
            this.DocCorGen.Location = new System.Drawing.Point(218, 111);
            this.DocCorGen.Name = "DocCorGen";
            this.DocCorGen.Size = new System.Drawing.Size(15, 14);
            this.DocCorGen.TabIndex = 17;
            this.DocCorGen.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(129, 327);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Batch Scan";
            // 
            // BatchBes
            // 
            this.BatchBes.AutoSize = true;
            this.BatchBes.Location = new System.Drawing.Point(291, 326);
            this.BatchBes.Name = "BatchBes";
            this.BatchBes.Size = new System.Drawing.Size(15, 14);
            this.BatchBes.TabIndex = 28;
            this.BatchBes.UseVisualStyleBackColor = true;
            // 
            // BatchGen
            // 
            this.BatchGen.AutoSize = true;
            this.BatchGen.Location = new System.Drawing.Point(218, 326);
            this.BatchGen.Name = "BatchGen";
            this.BatchGen.Size = new System.Drawing.Size(15, 14);
            this.BatchGen.TabIndex = 27;
            this.BatchGen.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(129, 299);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Photo";
            // 
            // PhotoBes
            // 
            this.PhotoBes.AutoSize = true;
            this.PhotoBes.Location = new System.Drawing.Point(291, 298);
            this.PhotoBes.Name = "PhotoBes";
            this.PhotoBes.Size = new System.Drawing.Size(15, 14);
            this.PhotoBes.TabIndex = 26;
            this.PhotoBes.UseVisualStyleBackColor = true;
            // 
            // PhotoGen
            // 
            this.PhotoGen.AutoSize = true;
            this.PhotoGen.Location = new System.Drawing.Point(218, 298);
            this.PhotoGen.Name = "PhotoGen";
            this.PhotoGen.Size = new System.Drawing.Size(15, 14);
            this.PhotoGen.TabIndex = 25;
            this.PhotoGen.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(129, 268);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "eSig";
            // 
            // eSigBes
            // 
            this.eSigBes.AutoSize = true;
            this.eSigBes.Location = new System.Drawing.Point(291, 267);
            this.eSigBes.Name = "eSigBes";
            this.eSigBes.Size = new System.Drawing.Size(15, 14);
            this.eSigBes.TabIndex = 24;
            this.eSigBes.UseVisualStyleBackColor = true;
            // 
            // eSigGen
            // 
            this.eSigGen.AutoSize = true;
            this.eSigGen.Location = new System.Drawing.Point(218, 267);
            this.eSigGen.Name = "eSigGen";
            this.eSigGen.Size = new System.Drawing.Size(15, 14);
            this.eSigGen.TabIndex = 23;
            this.eSigGen.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(129, 240);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "AdHoc";
            // 
            // AdHocBes
            // 
            this.AdHocBes.AutoSize = true;
            this.AdHocBes.Location = new System.Drawing.Point(291, 239);
            this.AdHocBes.Name = "AdHocBes";
            this.AdHocBes.Size = new System.Drawing.Size(15, 14);
            this.AdHocBes.TabIndex = 22;
            this.AdHocBes.UseVisualStyleBackColor = true;
            // 
            // AdHocGen
            // 
            this.AdHocGen.AutoSize = true;
            this.AdHocGen.Location = new System.Drawing.Point(218, 239);
            this.AdHocGen.Name = "AdHocGen";
            this.AdHocGen.Size = new System.Drawing.Size(15, 14);
            this.AdHocGen.TabIndex = 21;
            this.AdHocGen.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(114, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "General";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(133, 208);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "T24";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(288, 208);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 9;
            this.label15.Text = "Bespoke";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(215, 208);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Generic";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(363, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 13);
            this.label17.TabIndex = 38;
            this.label17.Text = "Other";
            // 
            // OtherApps
            // 
            this.OtherApps.Location = new System.Drawing.Point(366, 29);
            this.OtherApps.MaxLength = 500;
            this.OtherApps.Multiline = true;
            this.OtherApps.Name = "OtherApps";
            this.OtherApps.Size = new System.Drawing.Size(349, 352);
            this.OtherApps.TabIndex = 29;
            // 
            // statusBox
            // 
            this.statusBox.Location = new System.Drawing.Point(291, 546);
            this.statusBox.Name = "statusBox";
            this.statusBox.ReadOnly = true;
            this.statusBox.Size = new System.Drawing.Size(100, 20);
            this.statusBox.TabIndex = 40;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 169);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 13);
            this.label18.TabIndex = 42;
            this.label18.Text = "LN Version";
            // 
            // LNVerText
            // 
            this.LNVerText.Location = new System.Drawing.Point(6, 185);
            this.LNVerText.MaxLength = 10;
            this.LNVerText.Name = "LNVerText";
            this.LNVerText.Size = new System.Drawing.Size(67, 20);
            this.LNVerText.TabIndex = 10;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 13);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 13);
            this.label19.TabIndex = 44;
            this.label19.Text = "No. Prod Servers";
            // 
            // ProdSerText
            // 
            this.ProdSerText.Location = new System.Drawing.Point(6, 29);
            this.ProdSerText.MaxLength = 10;
            this.ProdSerText.Name = "ProdSerText";
            this.ProdSerText.Size = new System.Drawing.Size(67, 20);
            this.ProdSerText.TabIndex = 7;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 65);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(87, 13);
            this.label20.TabIndex = 46;
            this.label20.Text = "No. Test Servers";
            // 
            // TestSerText
            // 
            this.TestSerText.Location = new System.Drawing.Point(6, 81);
            this.TestSerText.MaxLength = 10;
            this.TestSerText.Name = "TestSerText";
            this.TestSerText.Size = new System.Drawing.Size(67, 20);
            this.TestSerText.TabIndex = 8;
            // 
            // passwordText
            // 
            this.passwordText.Location = new System.Drawing.Point(12, 43);
            this.passwordText.Name = "passwordText";
            this.passwordText.PasswordChar = '*';
            this.passwordText.Size = new System.Drawing.Size(90, 20);
            this.passwordText.TabIndex = 47;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(12, 27);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(83, 13);
            this.label21.TabIndex = 48;
            this.label21.Text = "Editor Password";
            // 
            // LogInButton
            // 
            this.LogInButton.Location = new System.Drawing.Point(108, 41);
            this.LogInButton.Name = "LogInButton";
            this.LogInButton.Size = new System.Drawing.Size(75, 23);
            this.LogInButton.TabIndex = 49;
            this.LogInButton.Text = "Log In";
            this.LogInButton.UseVisualStyleBackColor = true;
            this.LogInButton.Click += new System.EventHandler(this.LogInButton_Click);
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Location = new System.Drawing.Point(189, 46);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(10, 13);
            this.passwordLabel.TabIndex = 50;
            this.passwordLabel.Text = " ";
            // 
            // tableName
            // 
            this.tableName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tableName.FormattingEnabled = true;
            this.tableName.Items.AddRange(new object[] {
            "CustomerInformationDM",
            "CustomerInformationDMTEST"});
            this.tableName.Location = new System.Drawing.Point(563, 12);
            this.tableName.Name = "tableName";
            this.tableName.Size = new System.Drawing.Size(200, 21);
            this.tableName.TabIndex = 51;
            this.tableName.SelectedIndexChanged += new System.EventHandler(this.tableName_SelectedIndexChanged);
            // 
            // TabsWindow
            // 
            this.TabsWindow.Controls.Add(this.swDetails);
            this.TabsWindow.Controls.Add(this.customerDetails);
            this.TabsWindow.Location = new System.Drawing.Point(12, 70);
            this.TabsWindow.Name = "TabsWindow";
            this.TabsWindow.SelectedIndex = 0;
            this.TabsWindow.Size = new System.Drawing.Size(729, 641);
            this.TabsWindow.TabIndex = 52;
            // 
            // swDetails
            // 
            this.swDetails.Controls.Add(this.importFromFile);
            this.swDetails.Controls.Add(this.saveToFile);
            this.swDetails.Controls.Add(this.label26);
            this.swDetails.Controls.Add(this.notesText);
            this.swDetails.Controls.Add(this.label23);
            this.swDetails.Controls.Add(this.BPMVerText);
            this.swDetails.Controls.Add(this.label20);
            this.swDetails.Controls.Add(this.TestSerText);
            this.swDetails.Controls.Add(this.label19);
            this.swDetails.Controls.Add(this.ProdSerText);
            this.swDetails.Controls.Add(this.label18);
            this.swDetails.Controls.Add(this.LNVerText);
            this.swDetails.Controls.Add(this.statusBox);
            this.swDetails.Controls.Add(this.OtherApps);
            this.swDetails.Controls.Add(this.label17);
            this.swDetails.Controls.Add(this.label14);
            this.swDetails.Controls.Add(this.label15);
            this.swDetails.Controls.Add(this.label16);
            this.swDetails.Controls.Add(this.label13);
            this.swDetails.Controls.Add(this.label9);
            this.swDetails.Controls.Add(this.BatchBes);
            this.swDetails.Controls.Add(this.BatchGen);
            this.swDetails.Controls.Add(this.label10);
            this.swDetails.Controls.Add(this.PhotoBes);
            this.swDetails.Controls.Add(this.PhotoGen);
            this.swDetails.Controls.Add(this.label11);
            this.swDetails.Controls.Add(this.eSigBes);
            this.swDetails.Controls.Add(this.eSigGen);
            this.swDetails.Controls.Add(this.label12);
            this.swDetails.Controls.Add(this.AdHocBes);
            this.swDetails.Controls.Add(this.AdHocGen);
            this.swDetails.Controls.Add(this.label7);
            this.swDetails.Controls.Add(this.TreeBes);
            this.swDetails.Controls.Add(this.TreeGen);
            this.swDetails.Controls.Add(this.label8);
            this.swDetails.Controls.Add(this.DocCorBes);
            this.swDetails.Controls.Add(this.DocCorGen);
            this.swDetails.Controls.Add(this.label6);
            this.swDetails.Controls.Add(this.BSMBes);
            this.swDetails.Controls.Add(this.BSMGen);
            this.swDetails.Controls.Add(this.label5);
            this.swDetails.Controls.Add(this.GSABes);
            this.swDetails.Controls.Add(this.GSAGen);
            this.swDetails.Controls.Add(this.label3);
            this.swDetails.Controls.Add(this.label4);
            this.swDetails.Controls.Add(this.label2);
            this.swDetails.Controls.Add(this.label1);
            this.swDetails.Controls.Add(this.ClientVerText);
            this.swDetails.Controls.Add(this.DMVerText);
            this.swDetails.Controls.Add(this.pushChanges);
            this.swDetails.Location = new System.Drawing.Point(4, 22);
            this.swDetails.Name = "swDetails";
            this.swDetails.Padding = new System.Windows.Forms.Padding(3);
            this.swDetails.Size = new System.Drawing.Size(721, 615);
            this.swDetails.TabIndex = 0;
            this.swDetails.Text = "SoftwareDetails";
            this.swDetails.UseVisualStyleBackColor = true;
            // 
            // importFromFile
            // 
            this.importFromFile.Location = new System.Drawing.Point(538, 576);
            this.importFromFile.Name = "importFromFile";
            this.importFromFile.Size = new System.Drawing.Size(101, 25);
            this.importFromFile.TabIndex = 52;
            this.importFromFile.Text = "Import From File";
            this.importFromFile.UseVisualStyleBackColor = true;
            this.importFromFile.Click += new System.EventHandler(this.importFromFile_Click);
            // 
            // saveToFile
            // 
            this.saveToFile.Location = new System.Drawing.Point(424, 576);
            this.saveToFile.Name = "saveToFile";
            this.saveToFile.Size = new System.Drawing.Size(90, 25);
            this.saveToFile.TabIndex = 51;
            this.saveToFile.Text = "Save to File";
            this.saveToFile.UseVisualStyleBackColor = true;
            this.saveToFile.Click += new System.EventHandler(this.saveToFile_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 387);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 13);
            this.label26.TabIndex = 50;
            this.label26.Text = "Notes";
            // 
            // notesText
            // 
            this.notesText.AcceptsReturn = true;
            this.notesText.AcceptsTab = true;
            this.notesText.Location = new System.Drawing.Point(6, 403);
            this.notesText.MaxLength = 500;
            this.notesText.Multiline = true;
            this.notesText.Name = "notesText";
            this.notesText.Size = new System.Drawing.Size(709, 137);
            this.notesText.TabIndex = 30;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 221);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(68, 13);
            this.label23.TabIndex = 48;
            this.label23.Text = "BPM Version";
            // 
            // BPMVerText
            // 
            this.BPMVerText.Location = new System.Drawing.Point(6, 237);
            this.BPMVerText.MaxLength = 10;
            this.BPMVerText.Name = "BPMVerText";
            this.BPMVerText.Size = new System.Drawing.Size(67, 20);
            this.BPMVerText.TabIndex = 11;
            // 
            // customerDetails
            // 
            this.customerDetails.Controls.Add(this.outOfSupport);
            this.customerDetails.Controls.Add(this.createCustomer);
            this.customerDetails.Controls.Add(this.changeName);
            this.customerDetails.Controls.Add(this.label22);
            this.customerDetails.Controls.Add(this.newCustName);
            this.customerDetails.Location = new System.Drawing.Point(4, 22);
            this.customerDetails.Name = "customerDetails";
            this.customerDetails.Padding = new System.Windows.Forms.Padding(3);
            this.customerDetails.Size = new System.Drawing.Size(721, 615);
            this.customerDetails.TabIndex = 1;
            this.customerDetails.Text = "Customer Details";
            this.customerDetails.UseVisualStyleBackColor = true;
            // 
            // createCustomer
            // 
            this.createCustomer.Location = new System.Drawing.Point(126, 146);
            this.createCustomer.Name = "createCustomer";
            this.createCustomer.Size = new System.Drawing.Size(89, 38);
            this.createCustomer.TabIndex = 3;
            this.createCustomer.Text = "New Customer";
            this.createCustomer.UseVisualStyleBackColor = true;
            this.createCustomer.Click += new System.EventHandler(this.createCustomer_Click);
            // 
            // changeName
            // 
            this.changeName.Location = new System.Drawing.Point(31, 146);
            this.changeName.Name = "changeName";
            this.changeName.Size = new System.Drawing.Size(89, 38);
            this.changeName.TabIndex = 2;
            this.changeName.Text = "New Customer Name";
            this.changeName.UseVisualStyleBackColor = true;
            this.changeName.Click += new System.EventHandler(this.changeName_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(28, 51);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Name";
            // 
            // newCustName
            // 
            this.newCustName.Location = new System.Drawing.Point(31, 76);
            this.newCustName.Name = "newCustName";
            this.newCustName.Size = new System.Drawing.Size(150, 20);
            this.newCustName.TabIndex = 0;
            // 
            // lastChangedText
            // 
            this.lastChangedText.Location = new System.Drawing.Point(478, 44);
            this.lastChangedText.Name = "lastChangedText";
            this.lastChangedText.ReadOnly = true;
            this.lastChangedText.Size = new System.Drawing.Size(145, 20);
            this.lastChangedText.TabIndex = 53;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(279, 27);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 13);
            this.label24.TabIndex = 54;
            this.label24.Text = "Customer Name";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(475, 27);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(71, 13);
            this.label25.TabIndex = 55;
            this.label25.Text = "Last Updated";
            // 
            // outOfSupport
            // 
            this.outOfSupport.Location = new System.Drawing.Point(221, 146);
            this.outOfSupport.Name = "outOfSupport";
            this.outOfSupport.Size = new System.Drawing.Size(89, 38);
            this.outOfSupport.TabIndex = 4;
            this.outOfSupport.Text = "Out of Support";
            this.outOfSupport.UseVisualStyleBackColor = true;
            this.outOfSupport.Click += new System.EventHandler(this.outOfSupport_Click);
            // 
            // Form
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 723);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.lastChangedText);
            this.Controls.Add(this.tableName);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.LogInButton);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.passwordText);
            this.Controls.Add(this.TabsWindow);
            this.Controls.Add(this.customerList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form";
            this.Text = "Customer Data";
            this.TabsWindow.ResumeLayout(false);
            this.swDetails.ResumeLayout(false);
            this.swDetails.PerformLayout();
            this.customerDetails.ResumeLayout(false);
            this.customerDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox customerList;
        private System.Windows.Forms.Button pushChanges;
        private System.Windows.Forms.TextBox DMVerText;
        private System.Windows.Forms.TextBox ClientVerText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox GSAGen;
        private System.Windows.Forms.CheckBox GSABes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox BSMBes;
        private System.Windows.Forms.CheckBox BSMGen;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox TreeBes;
        private System.Windows.Forms.CheckBox TreeGen;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox DocCorBes;
        private System.Windows.Forms.CheckBox DocCorGen;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox BatchBes;
        private System.Windows.Forms.CheckBox BatchGen;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox PhotoBes;
        private System.Windows.Forms.CheckBox PhotoGen;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox eSigBes;
        private System.Windows.Forms.CheckBox eSigGen;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox AdHocBes;
        private System.Windows.Forms.CheckBox AdHocGen;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox OtherApps;
        private System.Windows.Forms.TextBox statusBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox LNVerText;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox ProdSerText;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox TestSerText;
        private System.Windows.Forms.TextBox passwordText;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button LogInButton;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.ComboBox tableName;
        private System.Windows.Forms.TabControl TabsWindow;
        private System.Windows.Forms.TabPage swDetails;
        private System.Windows.Forms.TabPage customerDetails;
        private System.Windows.Forms.Button createCustomer;
        private System.Windows.Forms.Button changeName;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox newCustName;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox BPMVerText;
        private System.Windows.Forms.TextBox lastChangedText;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox notesText;
        private System.Windows.Forms.Button saveToFile;
        private System.Windows.Forms.Button importFromFile;
        private System.Windows.Forms.Button outOfSupport;
    }
}

