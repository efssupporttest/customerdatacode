﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace CustomerDatabaseEntry
{
    public partial class Form : System.Windows.Forms.Form
    {
        //Credentials for database
        static string serverName = "vmappserver2"; 
        static string databaseName = "DeskPro";
        static string userName = "CustomerData";
        static string password = "c59YGs7gKS&A";
        static string connectionString = String.Format("Server={0};Database={1};User Id={2};Password={3};", serverName, databaseName, userName, password);

        public Form()
        {
            InitializeComponent();
            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(Form1_DragEnter);
            this.DragDrop += new DragEventHandler(Form1_DragDrop);
            enablePush("false"); //Disable pushing changes, adding new customers or changing names
            tableName.SelectedIndex = 0; //Prod, change to 1 for test
            loadList(tableName.Text); //Bring back list of customers from DB and fill in customerList
        }

        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            //Skip first line, read second one to importFields
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        void Form1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string file in files)
            {
                Console.WriteLine(file);
                var reader = new StreamReader(file);
                reader.ReadLine(); //skip first line
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    importFields(line);
                }
                reader.Close();
            }
            //var line = reader.ReadLine();
            //importFields(line);
        }

        private void customerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            statusBox.Text = "Loading";
            string name = customerList.Text; //Name of customer
            loadCustomerData(name);
        }

        private void loadCustomerData(string name)
        {
            newCustName.Text = name; //Fill in New Customer Name checkbox in case name needs to be changed
            Console.WriteLine("Name has changed to - " + name);
            writeLog("Returning results for " + name); //Write to log file to show name was searched for
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = String.Format("SELECT * FROM {0} where CustomerName = @nameValue", tableName.Text);
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@nameValue", name); //Return results for selected customer
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        //Run through results from query setting the columns to variables
                        string dmVer = string.Format("{0}", reader["DMVersion"]);
                        string clientVer = string.Format("{0}", reader["ClientVersion"]);
                        string LNVer = string.Format("{0}", reader["LNVersion"]);
                        string BPMver = string.Format("{0}", reader["BPMVersion"]);
                        string ProdServers = string.Format("{0}", reader["ProdServers"]);
                        string TestServers = string.Format("{0}", reader["TestServers"]);
                        string gsaGenVal = string.Format("{0}", reader["GSA_Generic"]);
                        string gsaBesVal = string.Format("{0}", reader["GSA_Bespoke"]);
                        string bsmGenVal = string.Format("{0}", reader["BSandM_Generic"]);
                        string bsmBesVal = string.Format("{0}", reader["BSandM_Bespoke"]);
                        string docCorGenVal = string.Format("{0}", reader["DocCorr_Generic"]);
                        string docCorBesVal = string.Format("{0}", reader["DocCorr_Bespoke"]);
                        string treeGenVal = string.Format("{0}", reader["TreeView_Generic"]);
                        string treeBesVal = string.Format("{0}", reader["TreeView_Bespoke"]);
                        string adHocGenVal = string.Format("{0}", reader["T24_AdHoc_Generic"]);
                        string adHocBesVal = string.Format("{0}", reader["T24_AdHoc_Bespoke"]);
                        string eSigGenVal = string.Format("{0}", reader["T24_eSig_Generic"]);
                        string eSigBesVal = string.Format("{0}", reader["T24_eSig_Bespoke"]);
                        string photoGenVal = string.Format("{0}", reader["T24_Photo_Generic"]);
                        string photoBesVal = string.Format("{0}", reader["T24_Photo_Bespoke"]);
                        string batchGenVal = string.Format("{0}", reader["T24_BatchScan_Generic"]);
                        string batchBesVal = string.Format("{0}", reader["T24_BatchScan_Bespoke"]);
                        string otherAppVal = string.Format("{0}", reader["Other"]);
                        string lastChangedVal = string.Format("{0}", reader["lastChanged"]);
                        string notesVal = string.Format("{0}", reader["notes"]);
                        //These are straight forward string fields
                        DMVerText.Text = dmVer.Trim();
                        ClientVerText.Text = clientVer.Trim();
                        LNVerText.Text = LNVer.Trim();
                        BPMVerText.Text = BPMver.Trim();
                        ProdSerText.Text = ProdServers.Trim();
                        TestSerText.Text = TestServers.Trim();
                        lastChangedText.Text = lastChangedVal.Trim();
                        notesText.Text = notesVal.Trim();

                        //These are tick boxes, method tickBox compares the table contents, if it is "Y" it ticks the box
                        tickBox(gsaGenVal, GSAGen);
                        tickBox(gsaBesVal, GSABes);
                        tickBox(bsmGenVal, BSMGen);
                        tickBox(bsmBesVal, BSMBes);
                        tickBox(docCorGenVal, DocCorGen);
                        tickBox(docCorBesVal, DocCorBes);
                        tickBox(treeGenVal, TreeGen);
                        tickBox(treeBesVal, TreeBes);
                        tickBox(adHocGenVal, AdHocGen);
                        tickBox(adHocBesVal, AdHocBes);
                        tickBox(eSigGenVal, eSigGen);
                        tickBox(eSigBesVal, eSigBes);
                        tickBox(photoGenVal, PhotoGen);
                        tickBox(photoBesVal, PhotoBes);
                        tickBox(batchGenVal, BatchGen);
                        tickBox(batchBesVal, BatchBes);

                        //In the database the app names are comma seperated, these are changed to new lines in app
                        string[] otherAppsArray = otherAppVal.Split(',');
                        OtherApps.Text = String.Empty;
                        for (int i = 0; i < otherAppsArray.Length; i++)
                        {
                            OtherApps.AppendText((otherAppsArray[i]).Trim());
                            OtherApps.AppendText("\r\n");
                        }
                    }
                    reader.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("SQL Error - " + ex);
                    System.Windows.Forms.Application.Exit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Code Error - " + ex);
                    System.Windows.Forms.Application.Exit();
                }

            }
            statusBox.Text = "Finished.";
        }

        private void pushChanges_Click_1(object sender, EventArgs e)
        {
            statusBox.Text = "Updating";
            writeLog("Changes pushed for customer \"" + customerList.Text + "\"");
            //These are strings and can be read directly from the text boxes
            string dmVer = (DMVerText.Text).Trim();
            string clientVer = (ClientVerText.Text).Trim();
            string lnVer = (LNVerText.Text).Trim();
            string bpmVer = (BPMVerText.Text).Trim();
            string prodServers = (ProdSerText.Text).Trim();
            string testServers = (TestSerText.Text).Trim();
            string notesVal = (notesText.Text).Trim();

            //Build the query string 
            string queryString = String.Format("update {0} set DMVersion = @DMVersionValue, ClientVersion = @ClientVersionValue, LNVersion = @LNVersionValue, BPMVersion = @BPMVersionValue, ProdServers = @ProdSerValue, TestServers = @TestSerValue, notes = @notesValue, GSA_Generic = @gsaGenVal, GSA_Bespoke = @gsaBesVal, BSandM_Generic = @bsmGenVal, BSandM_Bespoke = @bsmBesVal, DocCorr_Generic = @docCorGenVal, DocCorr_Bespoke = @docCorBesVal, TreeView_Generic = @treeGenVal, TreeView_Bespoke = @treeBesVal, T24_AdHoc_Generic = @adHocGenVal, T24_AdHoc_Bespoke = @adHocBesVal, T24_eSig_Generic = @eSigGenVal, T24_eSig_Bespoke = @eSigBesVal, T24_Photo_Generic = @photoGenVal, T24_Photo_Bespoke = @photoBesVal, T24_BatchScan_Generic = @batchGenVal, T24_BatchScan_Bespoke = @batchBesVal, Other = @otherAppsVal, lastChanged = @lastChangedVal where CustomerName = @NameValue", tableName.Text);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                DateTime myDateTime = DateTime.Now;
                command.Parameters.AddWithValue("@lastChangedVal", myDateTime.ToString("yyyy-MM-dd HH:mm:ss")); //Set the last updated key based off current date

                //Fill in string parameter list, if it is blank send a database null value
                if (dmVer != "")
                {
                    command.Parameters.AddWithValue("@DMVersionValue", dmVer);
                }
                else
                {
                    command.Parameters.AddWithValue("@DMVersionValue", DBNull.Value);
                }

                if (clientVer != "")
                {
                    command.Parameters.AddWithValue("@ClientVersionValue", clientVer);
                }
                else
                {
                    command.Parameters.AddWithValue("@ClientVersionValue", DBNull.Value);
                }

                if (lnVer != "")
                {
                    command.Parameters.AddWithValue("@LNVersionValue", lnVer);
                }
                else
                {
                    command.Parameters.AddWithValue("@LNVersionValue", DBNull.Value);
                }

                if (bpmVer != "")
                {
                    command.Parameters.AddWithValue("@BPMVersionValue", bpmVer);
                }
                else
                {
                    command.Parameters.AddWithValue("@BPMVersionValue", DBNull.Value);
                }

                if (prodServers != "")
                {
                    command.Parameters.AddWithValue("@ProdSerValue", prodServers);
                }
                else
                {
                    command.Parameters.AddWithValue("@ProdSerValue", DBNull.Value);
                }
                if (testServers != "")
                {
                    command.Parameters.AddWithValue("@TestSerValue", testServers);
                }
                else
                {
                    command.Parameters.AddWithValue("@TestSerValue", DBNull.Value);
                }

                if (notesVal != "")
                {
                    command.Parameters.AddWithValue("@notesValue", notesVal);
                }
                else
                {
                    command.Parameters.AddWithValue("@notesValue", DBNull.Value);
                }

                //Fill in customer name (can never be blank)
                command.Parameters.AddWithValue("@NameValue", customerList.Text);

                //Set the correct DB values for the apps. If the boxes are ticked send a Y, if not send a DB null
                if (GSAGen.Checked == true)
                {
                    command.Parameters.AddWithValue("@gsaGenVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@gsaGenVal", DBNull.Value);
                }

                if (GSABes.Checked == true)
                {
                    command.Parameters.AddWithValue("@gsaBesVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@gsaBesVal", DBNull.Value);
                }

                if (BSMGen.Checked == true)
                {
                    command.Parameters.AddWithValue("@bsmGenVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@bsmGenVal", DBNull.Value);
                }

                if (BSMBes.Checked == true)
                {
                    command.Parameters.AddWithValue("@bsmBesVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@bsmBesVal", DBNull.Value);
                }

                if (DocCorGen.Checked == true)
                {
                    command.Parameters.AddWithValue("@docCorGenVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@docCorGenVal", DBNull.Value);
                }

                if (DocCorBes.Checked == true)
                {
                    command.Parameters.AddWithValue("@docCorBesVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@docCorBesVal", DBNull.Value);
                }

                if (TreeGen.Checked == true)
                {
                    command.Parameters.AddWithValue("@treeGenVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@treeGenVal", DBNull.Value);
                }

                if (TreeBes.Checked == true)
                {
                    command.Parameters.AddWithValue("@treeBesVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@treeBesVal", DBNull.Value);
                }

                if (AdHocGen.Checked == true)
                {
                    command.Parameters.AddWithValue("@adHocGenVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@adHocGenVal", DBNull.Value);
                }

                if (AdHocBes.Checked == true)
                {
                    command.Parameters.AddWithValue("@adHocBesVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@adHocBesVal", DBNull.Value);
                }

                if (eSigGen.Checked == true)
                {
                    command.Parameters.AddWithValue("@eSigGenVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@eSigGenVal", DBNull.Value);
                }

                if (eSigBes.Checked == true)
                {
                    command.Parameters.AddWithValue("@eSigBesVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@eSigBesVal", DBNull.Value);
                }

                if (PhotoGen.Checked == true)
                {
                    command.Parameters.AddWithValue("@photoGenVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@photoGenVal", DBNull.Value);
                }

                if (PhotoBes.Checked == true)
                {
                    command.Parameters.AddWithValue("@photoBesVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@photoBesVal", DBNull.Value);
                }

                if (BatchGen.Checked == true)
                {
                    command.Parameters.AddWithValue("@batchGenVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@batchGenVal", DBNull.Value);
                }

                if (BatchBes.Checked == true)
                {
                    command.Parameters.AddWithValue("@batchBesVal", "Y");
                }
                else
                {
                    command.Parameters.AddWithValue("@batchBesVal", DBNull.Value);
                }
                //Other apps need to replace the new lines with commas for the database. 
                if (OtherApps.Text != "")
                {
                    string otherAppsVal = (OtherApps.Text).TrimEnd('\r', '\n');
                    string otherAppsSQL = Regex.Replace(otherAppsVal, @"[\t\n\r]+", ",");
                    command.Parameters.AddWithValue("@otherAppsVal", otherAppsSQL);
                }
                else
                {
                    command.Parameters.AddWithValue("@otherAppsVal", DBNull.Value);
                }

                try
                { 
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("SQL Error - " + ex);
                    System.Windows.Forms.Application.Exit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Code Error - " + ex);
                    System.Windows.Forms.Application.Exit();
                }
        }
            statusBox.Text = "Finished";
            loadCustomerData(customerList.Text);
        }

        //This is used to hash the password string for the login
        private string hashPassword(string password)
        {
            var sha1 = new SHA1CryptoServiceProvider();
            var sha1data = sha1.ComputeHash(Encoding.ASCII.GetBytes(password));
            var hashedPassword = System.Text.Encoding.Default.GetString(sha1data);
            return hashedPassword;
        }

        private void LogInButton_Click(object sender, EventArgs e)
        {
            //Since I can't be bothered to type the password in every time
            string username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            Console.WriteLine("Username is - " + username);
            if (System.Security.Principal.WindowsIdentity.GetCurrent().Name == "APS-CAMBRIDGE\\JasonG")
            {
                passwordLabel.Text = "Push Enabled";
                enablePush("true");
                string logonMessage = "AutoLogon for user " + username;
                writeLog(logonMessage);
                return;
            }
            string password = passwordText.Text;
            string hashedPassword = hashPassword(password);
            string message = "Logon attempt using password \"" + password + "\"";
            writeLog(message);
            if (hashedPassword == "EÒ£õ–ƒFT;´þD>*½ýï")
            {
                passwordLabel.Text = "Push Enabled";
                enablePush("true");
            }
            else
            {
                passwordLabel.Text = "Incorrect Password";
                enablePush("false");
            }
        }
        public void writeLog(string message)
        {
            string queryString = "insert into [LogTable] (time,name,event) values (@time,@name,@message)";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                SqlCommand command = new SqlCommand(queryString, connection);
                DateTime myDateTime = DateTime.Now;
                command.Parameters.AddWithValue("@time", myDateTime.ToString("yyyy-MM-dd HH:mm:ss"));
                command.Parameters.AddWithValue("@name", username);
                command.Parameters.AddWithValue("@message", message);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("SQL Error - " + ex);
                    System.Windows.Forms.Application.Exit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Code Error - " + ex);
                    System.Windows.Forms.Application.Exit();
                }
            }
        }
        //When the current table is changed update the customer list to reflect this
        private void tableName_SelectedIndexChanged(object sender, EventArgs e)
        {
            statusBox.Text = "Loading.";
            Console.WriteLine("Table Name is - " + tableName.Text);
            loadList(tableName.Text);
            statusBox.Text = "Finished.";
        }
        //If the customer name is changed
        private void changeName_Click(object sender, EventArgs e)
        {
            string customerName = (newCustName.Text).Trim();
            string oldCustomerName = customerList.Text;
            string message = "Customer renamed from" + oldCustomerName + " to " + customerName;
            writeLog(message);
            //Compare old and new customer names to make sure they are changed, if so display an error and then stop
            if (customerName == oldCustomerName)
            {
                MessageBox.Show("Name is the same");
                return;
            }
            else if (nameExists(customerName)) //Make sure name doesn't already exist in database
            {
                MessageBox.Show("Name already exists");
                return;
            }
            //Update customer name using Update statement
            string queryString = String.Format("UPDATE {0} set CustomerName = @customerName where CustomerName = @oldCustomerName", tableName.Text);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                SqlCommand command = new SqlCommand(queryString, connection);
                if (customerName != "" && customerName != oldCustomerName)
                {
                    command.Parameters.AddWithValue("@customerName", customerName);
                    command.Parameters.AddWithValue("@oldCustomerName", oldCustomerName);
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        reader.Close();
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show("SQL Error - " + ex);
                        System.Windows.Forms.Application.Exit();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Code Error - " + ex);
                        System.Windows.Forms.Application.Exit();
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Enter a valid customer name and try again");
                }
            }
            loadList(tableName.Text);
        }
        private Boolean nameExists(string name)
        {
            if (customerList.Items.Contains(name)) //Checks if name is already in combo box
            {
                return true;
            }
            else
            {
                return false; 
            }
        }
        //Create a new customer
        private void createCustomer_Click(object sender, EventArgs e)
        {
            string customerName = newCustName.Text;
            if (nameExists(customerName)) //Checks name isn't already in database
            {
                MessageBox.Show("Name already exists");
                return;
            }
            writeLog("New customer added with name - " + customerName);
            string queryString = String.Format("insert into {0} (CustomerName) Values (@customerName)", tableName.Text);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                SqlCommand command = new SqlCommand(queryString, connection);
                if (customerName != "")
                {
                    command.Parameters.AddWithValue("@customerName", customerName);
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        reader.Close();
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show("SQL Error - " + ex);
                        System.Windows.Forms.Application.Exit();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Code Error - " + ex);
                        System.Windows.Forms.Application.Exit();
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Enter a valid customer name and try again");
                }
            }
            loadList(tableName.Text);
        }


        private void enablePush(string enabled)
        {
            if (enabled == "true") //Enable all buttons that push changes
            {
                pushChanges.Enabled = true;
                changeName.Enabled = true;
                createCustomer.Enabled = true;
                tableName.Enabled = true;
                tableName.Visible = true;
                TabsWindow.TabPages.Add(customerDetails);
            }
            else
            {
                pushChanges.Enabled = false;
                changeName.Enabled = false;
                createCustomer.Enabled = false;
                tableName.Enabled = false;
                tableName.Visible = false;
                TabsWindow.TabPages.Remove(customerDetails);
            }
        }


        private void loadList(string databaseName)
        {
            customerList.Items.Clear();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string queryString = String.Format("SELECT CustomerName FROM {0} where outOfSupport is null order by CustomerName", tableName.Text);

                try
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read()) //Loop through customer names filling in combo box
                    {
                        string output = string.Format("{0}", reader["CustomerName"]);
                        customerList.Items.Add(output);
                    }
                    // Always call Close when done reading.
                    reader.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("SQL Error - " + ex);
                    System.Windows.Forms.Application.Exit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Code Error - " + ex);
                    System.Windows.Forms.Application.Exit();
                }
            }
            customerList.SelectedIndex = 0;
        }

        public void tickBox(string chechVal, System.Windows.Forms.CheckBox checkBox)
        {
            if (chechVal.Trim() == "Y")
            {
                checkBox.Checked = true;
            }
            else
            {
                checkBox.Checked = false;
            }
        }

        private void saveToFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "report|*.csv";
            saveFileDialog1.Title = "Save the report file";
            saveFileDialog1.FileName = customerList.Text;
            saveFileDialog1.ShowDialog();
            System.IO.StreamWriter fs = new System.IO.StreamWriter(saveFileDialog1.OpenFile());
            fs.WriteLine(buildReport());
            fs.Close();
        }

        private string buildReport()
        {
            //Builds CSV file for the report, can be improved by making it xml
            string returnString = "custName,dmVer,clientVer,lnVer,bpmVer,prodServers,testServers,notesVal,GSAGen,GSABes,BSMGen,BSMBes,DocCorGen,DocCorBes,TreeGen,TreeBes,AdHocGen,AdHocBes,eSigGen,eSigBes,PhotoGen,PhotoBes,BatchGen,BatchBes,OtherApps\r\n";
            returnString += customerList.Text;
            returnString += ",";
            returnString += (DMVerText.Text).Trim();
            returnString += ",";
            returnString += (ClientVerText.Text).Trim();
            returnString += ",";
            returnString += (LNVerText.Text).Trim();
            returnString += ",";
            returnString += (BPMVerText.Text).Trim();
            returnString += ",";
            returnString += (ProdSerText.Text).Trim();
            returnString += ",";
            returnString += (TestSerText.Text).Trim();
            returnString += ",";
            string notesreplaceText = Regex.Replace((notesText.Text).Trim(), @"[\t\n\r]+", ";");
            returnString += notesreplaceText.Replace(",", "<comma>"); //Commas are repalced so they don't interfere with the csv
            returnString += ",";

            returnString += returnCheckedValue(GSAGen);
            returnString += returnCheckedValue(GSABes);
            returnString += returnCheckedValue(BSMGen);
            returnString += returnCheckedValue(BSMBes);
            returnString += returnCheckedValue(DocCorGen);
            returnString += returnCheckedValue(DocCorBes);
            returnString += returnCheckedValue(TreeGen);
            returnString += returnCheckedValue(TreeBes);
            returnString += returnCheckedValue(AdHocGen);
            returnString += returnCheckedValue(AdHocBes);
            returnString += returnCheckedValue(eSigGen);
            returnString += returnCheckedValue(eSigBes);
            returnString += returnCheckedValue(PhotoGen);
            returnString += returnCheckedValue(PhotoBes);
            returnString += returnCheckedValue(BatchGen);
            returnString += returnCheckedValue(BatchBes);

            if (OtherApps.Text != "")
            {
                string otherAppsVal = (OtherApps.Text).TrimEnd('\r', '\n');
                returnString += Regex.Replace(otherAppsVal, @"[\t\n\r]+", ";");
            }
            else
            {
                returnString += "";
            }
            
            return returnString;
        }

        private string returnCheckedValue(CheckBox tickBox)
        {
            if (tickBox.Checked == true)
            {
                return "Y,";
            }
            else
            {
                return "N,";
            }
        }

        private void importFromFile_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog theDialog = new OpenFileDialog();
            theDialog.Title = "Open Report File";
            theDialog.Filter = "CSV files|*.csv";
            theDialog.InitialDirectory = @"C:\";
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = theDialog.OpenFile()) != null)
                    {
                        var reader = new StreamReader(myStream);
                        reader.ReadLine(); //skip first line
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            importFields(line);
                        }
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
                
            }
        }
        private void importFields(string line)
        {
            var values = line.Split(',');
            int index = customerList.Items.IndexOf(values[0]);
            customerList.SelectedIndex = index;
            DMVerText.Text = values[1];
            ClientVerText.Text = values[2];
            LNVerText.Text = values[3];
            BPMVerText.Text = values[4];
            ProdSerText.Text = values[5];
            TestSerText.Text = values[6];
            string notesValues = (values[7]).Replace("<comma>", ",");
            notesText.Text = notesValues.Replace(";", "\r\n");

            tickBox(values[8], GSAGen);
            tickBox(values[9], GSABes);
            tickBox(values[10], BSMGen);
            tickBox(values[11], BSMBes);
            tickBox(values[12], DocCorGen);
            tickBox(values[13], DocCorBes);
            tickBox(values[14], TreeGen);
            tickBox(values[15], TreeBes);
            tickBox(values[16], AdHocGen);
            tickBox(values[17], AdHocBes);
            tickBox(values[18], eSigGen);
            tickBox(values[19], eSigBes);
            tickBox(values[20], PhotoGen);
            tickBox(values[21], PhotoBes);
            tickBox(values[22], BatchGen);
            tickBox(values[23], BatchBes);

            OtherApps.Text = (values[24]).Replace(";", "\r\n");
        }

        private void outOfSupport_Click(object sender, EventArgs e)
        {
            string CustomerName = customerList.Text;
            string message = "Support cancelled for " + CustomerName;
            writeLog(message);

            //Update customer using Update statement
            string queryString = String.Format("UPDATE {0} set outOfSupport = '1' where CustomerName = @CustomerName", tableName.Text);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@customerName", CustomerName);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("SQL Error - " + ex);
                    System.Windows.Forms.Application.Exit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Code Error - " + ex);
                    System.Windows.Forms.Application.Exit();
                }
            }
            loadList(tableName.Text);
        }
    }
}
